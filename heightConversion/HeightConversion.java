package heightConversion;

import java.io.*;
import java.util.Scanner;

public class HeightConversion {

    public static void main(String args[]) {

        System.out.println("25 CM = "+convert(Units.CM, Units.DM,25) +" DM");
        System.out.println("200 KM = "+convert(Units.KM, Units.CM,200) +" CM");
        System.out.println("204.57 KM = "+convert(Units.KM, Units.M,204.57) +" M");
        System.out.println();

        System.out.println("Now from file:");
        readFromFile("C:\\Users\\ciure\\Desktop\\my projects\\project 1\\convert heights.txt");
    }

    private static double convert(Units fromUnit, Units toUnit, double height) {
        int difference = determineDifference(fromUnit, toUnit);
        height = height / Math.pow(10, difference);
        return height;
    }

    private static int determineDifference(Units fromUnit, Units toUnit) {
        Units[] units = new Units[]{Units.MM, Units.CM, Units.DM, Units.M, Units.DAM, Units.HM, Units.KM};

        int from=-20, to=-20;
        for (int i = 0; i < units.length; i++) {
            if (fromUnit.equals(units[i])) {
                from = i;
            } else if (toUnit.equals(units[i])) {
                to = i;
            }
        }
        return from-to;
    }

    public static void readFromFile (String fileName) {
        String height, from, to;
        try {
            File myObj = new File(fileName);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String thisLine = myReader.nextLine();
                height = thisLine.split(" ")[0];
                from = thisLine.split(" ")[1];
                to = thisLine.split(" ")[2];
                System.out.println(height + " "+from.toUpperCase()+ " = "+convert(Units.valueOf(from.toUpperCase()), Units.valueOf(to.toUpperCase()), Double.parseDouble(height))
                        + " " + to.toUpperCase());
            }
            myReader.close();
        }
        catch (FileNotFoundException e) {
            System.err.println("ERROR");
            e.printStackTrace();
        }
    }

    /*private static heightConversion.Units stringToEnum(String unit) {
        heightConversion.Units[] units = new heightConversion.Units[]{heightConversion.Units.MM, heightConversion.Units.CM, heightConversion.Units.DM, heightConversion.Units.M, heightConversion.Units.DAM, heightConversion.Units.HM, heightConversion.Units.KM};

        for (int i =0; i<units.length;i++) {
            if(unit.equalsIgnoreCase(units[i].toString())) {
                return units[i];
            }
        }
        return Uni
    }*/
}

enum Units {
    MM,
    CM,
    DM,
    M,
    DAM,
    HM,
    KM
}
