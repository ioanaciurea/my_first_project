package clinicManagement;

import java.time.LocalDateTime;

public class Appointment {
    private String firstName;
    private String lastName;
    private String doctor;
    private LocalDateTime appointmentDateTime;

    public Appointment(String firstName, String lastName, String doctor, LocalDateTime appointmentDateTime) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.doctor = doctor;
        this.appointmentDateTime = appointmentDateTime;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getDoctor() {
        return doctor;
    }

    public LocalDateTime getAppointmentDateTime() {
        return appointmentDateTime;
    }

    @Override
    public String toString() {
        return "Appointment{ " +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", doctor='" + doctor + '\'' +
                ", appointmentDateTime=" + appointmentDateTime +
                " }";
    }
}
