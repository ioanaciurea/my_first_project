package clinicManagement;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ClinicCalendar {
    private List<Appointment> calendar;

    public ClinicCalendar(List<Appointment> calendar) {
        this.calendar = calendar;
    }

    public ClinicCalendar() {
        this.calendar = new ArrayList<>();
    }

    public void addAppointment(String firstName, String secondName, String doctor, String dateTime) {
        LocalDateTime localDateTime;
        try {
            localDateTime = LocalDateTime.parse(dateTime.toUpperCase(), DateTimeFormatter.ofPattern("d/M/yyyy h:mm a"));
        } catch (Throwable t) {
            throw new RuntimeException("Unable to create date-time, please enter with format[d/M/yyyy h:mm a]");
        }

        Appointment appointment = new Appointment(firstName, secondName, doctor, localDateTime);
        calendar.add(appointment);
    }

    public List<Appointment> getCalendar() {
        return calendar;
    }

    @Override
    public String toString() {
        String allAppointments = "The Clinic Calendar:\n";
        for (Appointment ap : calendar) {
            allAppointments += ap;
            allAppointments += '\n';
        }
        return allAppointments;
    }
}
