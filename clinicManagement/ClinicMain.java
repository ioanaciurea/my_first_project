package clinicManagement;

public class ClinicMain {

    public static void main(String[] args) {
        ClinicCalendar calendar = new ClinicCalendar();
        calendar.addAppointment("Ioana", "Ciurea", "Popescu Maria", "26/09/2022 8:00 am");
        calendar.addAppointment("Marcel", "Miron", "Popescu Maria", "30/09/2022 10:45 am");
        System.out.println(calendar.toString());
    }
}
