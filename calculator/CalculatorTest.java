package calculator;

import org.junit.jupiter.api.Test;

import static calculator.Calculator.multipleOperations;
import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {
    @Test
    public void multipleOperationsTest_Basic() {
        assertEquals(7, multipleOperations("3 - 4 + 8"));
        assertEquals(12.92, multipleOperations("8 + 6.42 - 1.5"));
        assertEquals(13.73, multipleOperations("8.5 - 4.7 + 4.13 + 5.8"));
    }

    @Test
    public void multipleOperationsTest_MultiplyDivide() {
        assertEquals(4, multipleOperations("20 - 4 / 2 * 5 / 1 - 6"));
        assertEquals(13, multipleOperations("5 + 6 * 2 - 8 / 2"));
        assertEquals(23, multipleOperations("5 + 6 * 3"));
        assertEquals(4, multipleOperations("16 / 8 * 3 * 5 / 6 - 1"));
    }

    @Test
    public void multipleOperationsTest_MultipleDecimals() {
        assertEquals("3.893", String.format("%.3f", multipleOperations("16 / 6.3 * 3.2 / 2 - 0.17")));
        assertEquals("-11.150", String.format("%.3f", multipleOperations("5.3 - 8 - 7 + 2 - 3.45")));
    }

    @Test
    public void multipleOperationsTest_FirstNumberNegative() {
        assertEquals(2, multipleOperations("-8 + 3 + 7"));
        assertEquals(-12, multipleOperations("-12 * 2 / 6 * 3"));
    }
}