package calculator;

import java.util.Scanner;

public class Calculator {
    public static void main(String args[]) {
        justOneOperation();
        //multipleOperations("20 - 4 / 2 * 5 / 1 - 6");
    }

    private static void justOneOperation() {
        System.out.println("Hello! Type your first value:");
        Scanner in = new Scanner(System.in);
        double a = in.nextDouble();
        System.out.println("Type your operator  +  -  *  /  :");
        String operator = in.next();
        System.out.println("Type your second value:");
        double b = in.nextDouble();
        System.out.println(a + " " + operator + " " + b + " = " + calculate(a, b, operator));
    }

    private static double calculate(double a, double b, String operator) {
        double result = 0;
        switch (operator) {
            case "+": {
                result = a + b;
                break;
            }
            case "-": {
                result = a - b;
                break;
            }
            case "*": {
                result = a * b;
                break;
            }
            case "/": {
                result = a / b;
                break;
            }
            default:
                System.out.println("You didn't type a correct operator !!!");
                break;
        }
        return result;
    }

    protected static double calcSuccessively(String[] elements, int lengthOfOperation) {
        double res = Double.parseDouble(elements[0]);
        for (int i = 1; i < lengthOfOperation; i = i + 2) {
            res = calculate(res, Double.parseDouble(elements[i + 1]), elements[i]);
        }
        return res;
    }

    protected static double multipleOperations(String op) {

        String[] elements = op.split("\\s");
        double result = 0;
        int[] principalOp = new int[elements.length];
        int j = 0, newElementsLength = elements.length;

        for (int i = 0; i < elements.length; i++) {
            if (elements[i].equals("*") || elements[i].equals("/")) {
                principalOp[j] = i;
                j++;
            }
        }

        if (j == 0) { // if we don't have * or / operators
            result = calcSuccessively(elements, elements.length);
        } else { //otherwise we have to calculate * and / first
            for (int i = 0; i < j; i++) {
                result = calculate(Double.parseDouble(elements[principalOp[i] - 1]), Double.parseDouble(elements[principalOp[i] + 1]), elements[principalOp[i]]);
                elements[principalOp[i] - 1] = String.valueOf(result);
                //move elements of operation
                for (int n = principalOp[i]; n + 2 < elements.length; n++) {
                    elements[n] = elements[n + 2];
                }
                newElementsLength = newElementsLength - 2;

                for (int x = i + 1; x < j; x++) {
                    principalOp[x] = principalOp[x] - 2;
                }
            }
            //after calculation of * and /
            result = calcSuccessively(elements, newElementsLength);
        }
        return result;
    }
}
